import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {

  // @Input() numeroHijo = 1;
  // @Input() edad = 0;
  // fecha = new Date().toDateString();
  // @Input() ciudad: string = " "; 
  // @Output() pedirPermiso: EventEmitter<boolean|string> = new EventEmitter()

  @Input() anioHijo;
  @Output() respuestaEdad: EventEmitter<string> = new EventEmitter()


  constructor() { }

  ngOnInit() {
    // console.log('Se construye el hijo',this.pedirPermiso)
    // this.pedirPermiso.emit('Hola')
    if (this.anioHijo) {
      this.edadDelHijo(this.anioHijo)
    }
  }

  edadDelHijo(anio: number) {
    const edadHijoEntregar = 2018 - anio;
    this.respuestaEdad.emit(`La edad del hijo es ${edadHijoEntregar}`)
  }
}
