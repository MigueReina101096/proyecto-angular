import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  anioPadre = 1996;

  constructor() { }

  ngOnInit() {
  }
  escucharHijo(evento){
    console.log('Peticion del hijo', evento)
  }
}
